
tdaq_package()

tdaq_add_library(compression 
  src/*.cxx
  LINK_LIBRARIES ers ZLIB)

tdaq_add_executable(comp_test 
  NOINSTALL 
  test/comp_test.cxx 
  LINK_LIBRARIES compression Boost::unit_test_framework)

add_test(NAME comp_test COMMAND comp_test --build_info=yes --log_level=test_suite)
