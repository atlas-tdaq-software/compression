
#include "compression/compression.h"
#include "zlib.h"
#include <boost/assert.hpp>

namespace compression{
  std::string zmsg(const z_stream& strm){
    std::string zmsg("None");
    if(strm.msg){
      zmsg = strm.msg;
    }
    return zmsg;
  }
}

void compression::zlibcompress(compression::CompressionBuffer& compressed,
			       uint32_t& compressedsize,
			       const uint32_t& entries, 
			       const struct iovec* iov,
			       const uint32_t& totalsize,
			       const uint32_t& level){
  
  if(compressed.buffersize() < totalsize*1.1){
    compressed.realloc(totalsize*1.1);
  }
  
  z_stream strm;
  
  strm.zalloc = Z_NULL;
  strm.zfree = Z_NULL;
  strm.opaque = Z_NULL;
  auto ret = ::deflateInit(&strm, level); //level

  if(ret != Z_OK){
    compression::ZlibFailed issue(ERS_HERE, "Zlib init failed", ret, "none");
    throw issue;
  }
 
  uint32_t compsize = 0;
  uint32_t available = compressed.buffersize();
  strm.avail_out = available;
  strm.next_out = reinterpret_cast<Bytef*>(compressed.handle());
  
  for (unsigned int i=0; i < entries; ++i) {

    strm.next_in = static_cast<Bytef*>(iov[i].iov_base);
    strm.avail_in = iov[i].iov_len;
  
    int flush = ((i+1)==entries) ? Z_FINISH : Z_NO_FLUSH;

    while(true){
      ret = ::deflate(&strm, flush);
      
      if(ret == Z_STREAM_ERROR){
	compression::ZlibFailed issue(ERS_HERE, "Zlib deflate failed", 
				      ret, compression::zmsg(strm));
	throw issue;
      }

      if(strm.avail_out){ 
	break;
      }else{
	compsize += available;
	
	//Double available buffer space
	compressed.grow(2*compressed.buffersize());
	available = compressed.buffersize()/2;
	strm.avail_out = available;
	strm.next_out = static_cast<Bytef*>(compressed.handle()) +available;
      }
    }
  }

  compsize += (available-strm.avail_out);
  ret = ::deflateEnd(&strm);

  if(ret != Z_OK){
    compression::ZlibFailed issue(ERS_HERE, "Zlib deflateEnd failed", 
				  ret, compression::zmsg(strm));
    throw issue;
  }

  //Pad @4byte with zeros:
  uint32_t tobeadded = sizeof(uint32_t)-(compsize%sizeof(uint32_t));
  if(tobeadded>0 && tobeadded<sizeof(uint32_t)){
    
    if(tobeadded>(compressed.buffersize()-compsize)){
      //Grow buffer
      compressed.grow(compsize+tobeadded);
    }
    
    ::bzero(static_cast<Bytef*>(compressed.handle())+compsize,tobeadded);
    compsize += tobeadded;
  }
  
  compressedsize = compsize;
}

void
compression::zlibuncompress(compression::CompressionBuffer& uncompressed,
			    uint32_t& uncompressedsize,
			    const void * compressed,
			    const uint32_t& compsize){
  
  uLong bufsize;
  int ret;
    
  if(uncompressed.buffersize() < compsize*3){
    uncompressed.realloc(compsize*3);
  }

  while(true){
    
    bufsize = uncompressed.buffersize();
    
    ret = ::uncompress(reinterpret_cast<Bytef*>(uncompressed.handle()), 
		       &bufsize,
		       static_cast<const Bytef*>(compressed), 
		       compsize);

    if(ret == Z_OK){
      uncompressedsize = bufsize;
      break;
    }else if(ret == Z_DATA_ERROR){
      ::memcpy(uncompressed.handle(), compressed, compsize);
      uncompressedsize = compsize;

      compression::ZlibFailed 
	issue(ERS_HERE, 
	      "Zlib uncompression failed with Z_DATA_ERROR", 
	      ret, "none");
      throw issue;
      
    }else if(ret == Z_MEM_ERROR){
      //Not enough memory. Not much to do here!
      ERS_LOG("Zlib uncompressed failed with MEM_ERROR. Code: " << ret);
      ERS_ASSERT(ret != Z_MEM_ERROR);
    }else if(ret == Z_BUF_ERROR){
      //We need a bigger buffer
      uncompressed.realloc(uncompressed.buffersize()*2);
    }
  }
}

