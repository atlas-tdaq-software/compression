
#include "compression/DataBuffer.h"

#include <string.h>

class compression::DataBuffer::implementation{

public:
  
  implementation():
    m_buffer(nullptr),
    m_buffersize(0){}

  implementation(const uint32_t& size):
    m_buffer(nullptr),
    m_buffersize(0){
    m_buffer.reset(new char[size]);
    m_buffersize = size;
  }
 
  ~implementation()
  {}

  
  void realloc(const uint32_t& newsize)
  {
    m_buffer.reset(new char[newsize]);
    m_buffersize = newsize;
  }

  void grow(const uint32_t& newsize)
  {
    if(newsize <=  m_buffersize)
      return;
    
    auto old_buffer = std::move(m_buffer);
    m_buffer.reset(new char[newsize]);
    ::memcpy(m_buffer.get(), old_buffer.get(), m_buffersize);
    m_buffersize = newsize;
  }

  void * handle()
  { return m_buffer.get(); }

  uint32_t buffersize() const
  { return m_buffersize; }

private:
  std::unique_ptr<char[]> m_buffer;
  uint32_t m_buffersize;

};

compression::DataBuffer::DataBuffer():
  rep(new implementation())
{}

compression::DataBuffer::DataBuffer(const uint32_t& size):
  rep(new implementation(size))
{}

compression::DataBuffer::~DataBuffer()
{ delete rep; }
     
void compression::DataBuffer::realloc(const uint32_t& newsize)
{ return rep->realloc(newsize); }

void compression::DataBuffer::grow(const uint32_t& newsize)
{ return rep->grow(newsize); }

void* compression::DataBuffer::handle()
{ return rep->handle(); }
    
uint32_t compression::DataBuffer::buffersize() const 
{ return rep->buffersize(); }
