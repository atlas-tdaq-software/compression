//Dear emacs, this is -*- c++ -*-

/**
 * @file write_test.cxx
 * @author <a href="mailto:Wainer.Vandelli@cern.ch">Wainer.Vandelli@cern.ch</a> 
 * $Author: $
 * $Revision: $
 * $Date: $
 * 
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Compression Test
#define BOOST_TEST_MAIN

#include <boost/test/unit_test.hpp>

#include <stdio.h>
#include <strings.h>
#include <iostream>

#include "compression/compression.h"
#include "compression/DataBuffer.h"

#define COMPDATA 0xFFAABBFF
static const uint32_t size = 1000000;

BOOST_AUTO_TEST_CASE( compress_data )
{

  auto event = new unsigned char[size];
  
  for(unsigned int i=0; i<(size/sizeof(uint32_t)); i++){
    ((uint32_t*)event)[i] = COMPDATA;
  }

  uint32_t iovsize = 2;
  auto iov = new struct iovec[iovsize];

  iov[0].iov_base = event;
  iov[0].iov_len = size/2;
  iov[1].iov_base = event+size/2;
  iov[1].iov_len = size/2;

  uint32_t compsize = 0;

  compression::DataBuffer comp(0);

  compression::zlibcompress(comp,
			    compsize,
			    iovsize, iov,
			    size,1);

  BOOST_CHECK(compsize < size);
  BOOST_CHECK(comp.buffersize()> 0);
  BOOST_CHECK(comp.buffersize()>= compsize);
  
  
  uint32_t decompsize = 0;

  compression::DataBuffer decomp(0);
  
  compression::zlibuncompress(decomp,
			      decompsize,
			      comp.handle(),
			      compsize);

  BOOST_CHECK_EQUAL(decompsize, size);
  
  for(unsigned int i=0; i<decompsize; i++){
    BOOST_CHECK_EQUAL(event[i],((unsigned char*)decomp.handle())[i]);
  }

  BOOST_CHECK_THROW(compression::zlibcompress(comp,
					      compsize,
					      iovsize, iov,
					      size,100), 
		    compression::ZlibFailed);
  
  delete[] iov;
  delete[] event;
}


BOOST_AUTO_TEST_CASE( uncompress_wrong_data )
{

  auto event = new unsigned char[size];
  
  for(unsigned int i=0; i<(size/sizeof(uint32_t)); i++){
    ((uint32_t*)event)[i] = COMPDATA;
  }

  uint32_t decompsize = 0;

  compression::DataBuffer decomp(0);

  BOOST_CHECK_THROW(compression::zlibuncompress(decomp,
						decompsize,
						event,
						size),
		    compression::ZlibFailed);

  
  BOOST_CHECK_EQUAL(decompsize,size);
  
  delete[] event;
}

